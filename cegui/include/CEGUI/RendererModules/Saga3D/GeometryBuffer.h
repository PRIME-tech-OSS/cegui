/***********************************************************************
    created:    Thu Dec 27 2018
    author:     Manh Nguyen Tien
*************************************************************************/
/***************************************************************************
 *   Copyright (C) 2004 - 2010 Paul D Turner & The CEGUI Development Team
 *
 *   Permission is hereby granted, free of charge, to any person obtaining
 *   a copy of this software and associated documentation files (the
 *   "Software"), to deal in the Software without restriction, including
 *   without limitation the rights to use, copy, modify, merge, publish,
 *   distribute, sublicense, and/or sell copies of the Software, and to
 *   permit persons to whom the Software is furnished to do so, subject to
 *   the following conditions:
 *
 *   The above copyright notice and this permission notice shall be
 *   included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 *   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 *   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *   OTHER DEALINGS IN THE SOFTWARE.
 ***************************************************************************/
#ifndef _CEGUISaga3DGeometryBuffer_h_
#define _CEGUISaga3DGeometryBuffer_h_

#include "../../GeometryBuffer.h"
#include "CEGUI/RendererModules/Saga3D/Renderer.h"
#include "../../Rect.h"
#include "../../Colour.h"
#include "../../Vertex.h"
#include "../../Quaternion.h"
#include <Saga.h>

#include <utility>
#include <vector>

#if defined(_MSC_VER)
#   pragma warning(push)
#   pragma warning(disable : 4251)
#endif

// Start of CEGUI namespace section
namespace CEGUI
{
//! Implementation of CEGUI::GeometryBuffer for the Saga3D engine
class SAGA3D_GUIRENDERER_API Saga3DGeometryBuffer : public GeometryBuffer
{
public:
    //! Constructor
    Saga3DGeometryBuffer(Saga3DRenderer& owner);
    //! Destructor
    virtual ~Saga3DGeometryBuffer();

    const auto& getMatrix() const { return d_matrix; }

    // implement CEGUI::GeometryBuffer interface.
    void draw() const;
    void setTranslation(const Vector3f& v);
    void setRotation(const Quaternion& r);
    void setPivot(const Vector3f& p);
    void setClippingRegion(const Rectf& region);
    void appendVertex(const Vertex& vertex);
    void appendGeometry(const Vertex* const vbuff, uint vertex_count);
    void setActiveTexture(Texture* texture);
    void reset();
    Texture* getActiveTexture() const;
    uint getVertexCount() const;
    uint getBatchCount() const;
    void setRenderEffect(RenderEffect* effect);
    RenderEffect* getRenderEffect();
    void setClippingActive(const bool active);
    bool isClippingActive() const;

protected:
    void updateMatrix() const;
    void updateBuffer();

    struct Saga3DVertex
    {
        glm::vec3 position;
        glm::vec2 uv;
        glm::vec4 color;
    };

    //! Type to track info for per-texture sub batches of geometry
    struct BatchInfo
    {
        saga::video::TextureHandle texture;
        std::uint32_t vertexCount;
        bool clip;
    };
    //! Saga3DRenderer object that owns this geometry buffer
    Saga3DRenderer& d_owner;
    //! Saga's video driver we're to use.
    saga::video::IVideoDriver& d_driver;
    //! Saga's scene manager
    saga::scene::ISceneManager& d_smgr;
    //! Geometry's node
    std::shared_ptr<saga::scene::ISceneNode> d_node;
    //! Geometry's mesh
    std::shared_ptr<saga::scene::IMesh> d_mesh;
    //! Geometry's mesh buffer
    saga::video::ShaderBufferHandle d_buffer;
    //! Texture that is set as active
    Saga3DTexture* d_activeTexture;
    //! type of container that tracks BatchInfos.
    typedef std::vector<BatchInfo> BatchList;
    //! rectangular clip region
    Rectf d_clipRect;
    //! whether clipping will be active for the current batch
    bool d_clippingActive;
    //! translation vector
    Vector3f d_translation;
    //! rotation quaternion
    Quaternion d_rotation;
    //! pivot point for rotation
    Vector3f d_pivot;
    //! model matrix cache
    mutable glm::mat4 d_matrix;
    //! true when d_matrix is valid and up to date
    mutable bool d_matrixValid;
    //! RenderEffect that will be used by the GeometryBuffer
    RenderEffect* d_effect;
    //! Type of container used to queue the geometry
    typedef std::vector<Saga3DVertex> VertexList;
    //! Type of container used for indexes
    typedef std::vector<std::uint32_t> IndexList;
    //! List of texture batches added to the geometry buffer
    BatchList d_batches;
    //! Container where added geometry is stored.
    VertexList d_vertices;
};


} // End of  CEGUI namespace section

#if defined(_MSC_VER)
#   pragma warning(pop)
#endif

#endif  // end of guard _CEGUISaga3DGeometryBuffer_h_
