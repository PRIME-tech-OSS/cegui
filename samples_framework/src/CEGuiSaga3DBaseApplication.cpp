/***********************************************************************
created:    Fri Dec 28 2018
author:     Manh Nguyen Tien
*************************************************************************/
/***************************************************************************
*   Copyright (C) 2004 - 2009 Paul D Turner & The CEGUI Development Team
*
*   Permission is hereby granted, free of charge, to any person obtaining
*   a copy of this software and associated documentation files (the
*   "Software"), to deal in the Software without restriction, including
*   without limitation the rights to use, copy, modify, merge, publish,
*   distribute, sublicense, and/or sell copies of the Software, and to
*   permit persons to whom the Software is furnished to do so, subject to
*   the following conditions:
*
*   The above copyright notice and this permission notice shall be
*   included in all copies or substantial portions of the Software.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
*   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
*   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
*   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
*   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
*   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
*   OTHER DEALINGS IN THE SOFTWARE.
***************************************************************************/
#ifdef HAVE_CONFIG_H
#   include "config.h"
#endif

#if defined(__linux__) || defined(__FreeBSD__) || defined(__NetBSD__) || defined(__HAIKU__)
# include <unistd.h>
#endif

// this controls conditional compile of file for Apple
#include "CEGUISamplesConfig.h"
#ifdef CEGUI_SAMPLES_RENDERER_SAGA3D_ACTIVE

#include "CEGuiSaga3DBaseApplication.h"
#include "SamplesFrameworkBase.h"
#include "CEGUI/RendererModules/Saga3D/Renderer.h"
#include "CEGUI/RendererModules/Saga3D/ImageCodec.h"
#include "CEGUI/System.h"
#include "CEGUI/GUIContext.h"

//----------------------------------------------------------------------------//
CEGuiSaga3DBaseApplication::CEGuiSaga3DBaseApplication() :
    d_lastDisplaySize( static_cast<const float>(s_defaultWindowWidth), static_cast<const float>(s_defaultWindowHeight) )
{
    // create a device
    d_device = saga::createDevice(saga::video::E_DRIVER_TYPE::VULKAN,
        { s_defaultWindowWidth, s_defaultWindowHeight },
        16, false, false, false);

    // get driver and scenemanager
    d_driver = d_device->getVideoDriver();
    d_smgr = d_device->getSceneManager();

    d_lastTime = d_device->getTime();

    // create irrlicht renderer, image codec and resource provider.
    CEGUI::Saga3DRenderer& renderer =
        CEGUI::Saga3DRenderer::create(*d_device);

    d_renderer = &renderer;
    d_imageCodec = &renderer.createSaga3DImageCodec(*d_driver);
    // d_resourceProvider =
        // &renderer.createSaga3DResourceProvider(*d_device->getFileSystem());
    // d_eventPusher = renderer.getEventPusher();
}

//----------------------------------------------------------------------------//
CEGuiSaga3DBaseApplication::~CEGuiSaga3DBaseApplication()
{
    /*
    if (d_device)
        d_device->drop();
        */
}

//----------------------------------------------------------------------------//
void CEGuiSaga3DBaseApplication::destroyRenderer()
{
    /*
    CEGUI::Saga3DRenderer& renderer =
        *static_cast<CEGUI::Saga3DRenderer*>(d_renderer);

    renderer.destroySaga3DResourceProvider(
        *static_cast<CEGUI::Saga3DResourceProvider*>(d_resourceProvider));

    renderer.destroySaga3DImageCodec(
        *static_cast<CEGUI::Saga3DImageCodec*>(d_imageCodec));

    CEGUI::Saga3DRenderer::destroy(renderer);
    */
}

//----------------------------------------------------------------------------//
void CEGuiSaga3DBaseApplication::run()
{
    CEGUI::System& guiSystem = CEGUI::System::getSingleton();

    // draw everything
    while (d_device->run())
    {
        // draw only if the window is active
        // if (d_device->isWindowActive())
        // {
            checkWindowResize();

            // calculate time elapsed
            const std::uint32_t currTime = d_device->getTime();
            const float elapsed =
                static_cast<float>(currTime - d_lastTime) / 1000.0f;
            d_lastTime = currTime;

            renderSingleFrame(elapsed);
        // } else
            // d_device->yield();

        // see if we should quit
        if (d_sampleApp->isQuitting())
            d_device->closeDevice();
    }
}

//----------------------------------------------------------------------------//
void CEGuiSaga3DBaseApplication::destroyWindow()
{

}

//----------------------------------------------------------------------------//
void CEGuiSaga3DBaseApplication::beginRendering(const float /*elapsed*/)
{

}

//----------------------------------------------------------------------------//
void CEGuiSaga3DBaseApplication::endRendering()
{
    static_cast<CEGUI::Saga3DRenderer*>(d_renderer)->present();
}

//----------------------------------------------------------------------------//
void CEGuiSaga3DBaseApplication::onEvent(const SDL_Event& event)
{
    /*
    return (d_renderer != 0) ?
        processEvent(event) :
    */
}

//----------------------------------------------------------------------------//
void CEGuiSaga3DBaseApplication::processEvent(const SDL_Event& event)
{
    /*
    switch (event.EventType)
    {
    case EET_KEY_INPUT_EVENT :
        if (event.KeyInput.PressedDown)
            return OnKeyDown(event.KeyInput.Key, event.KeyInput.Char, event.KeyInput.Control, event.KeyInput.Shift);
        else
            return OnKeyUp(event.KeyInput.Key, event.KeyInput.Char, event.KeyInput.Control, event.KeyInput.Shift);
        break;

    case EET_MOUSE_INPUT_EVENT :
        return OnMouse(event.MouseInput.X, event.MouseInput.Y, event.MouseInput.Wheel, event.MouseInput.Event);
        break;

    default:
        break;
    }

    return false;
    */
}

// //----------------------------------------------------------------------------//
// bool CEGuiSaga3DBaseApplication::OnKeyDown(EKEY_CODE key, wchar_t wch, bool /*ctrl*/, bool /*shift*/)
// {
//     bool handled = false;
//     handled = d_sampleApp->injectKeyDown(d_eventPusher->getKeyCode(key));
//     handled = d_sampleApp->injectChar(wch) || handled;
//     return true;
// }
// //----------------------------------------------------------------------------//
// bool CEGuiSaga3DBaseApplication::OnKeyUp(EKEY_CODE key, wchar_t /*wch*/, bool /*ctrl*/, bool /*shift*/)
// {
//     bool handled = false;
//     handled = d_sampleApp->injectKeyUp(d_eventPusher->getKeyCode(key));
//     return handled;
// }
// //----------------------------------------------------------------------------//
// bool CEGuiSaga3DBaseApplication::OnMouse(s32 x, s32 y, f32 w, EMOUSE_INPUT_EVENT e)
// {
//     bool handled = false;

//     switch (e)
//     {
//         //! Left mouse button was pressed down.
//     case EMIE_LMOUSE_PRESSED_DOWN:
//         handled = d_sampleApp->injectMouseButtonDown(CEGUI::LeftButton);
//         break;
//         //! Right mouse button was pressed down.
//     case EMIE_RMOUSE_PRESSED_DOWN:
//         handled = d_sampleApp->injectMouseButtonDown(CEGUI::RightButton);
//         break;
//         //! Middle mouse button was pressed down.
//     case EMIE_MMOUSE_PRESSED_DOWN:
//         handled = d_sampleApp->injectMouseButtonDown(CEGUI::MiddleButton);
//         break;
//         //! Left mouse button was left up.
//     case EMIE_LMOUSE_LEFT_UP:
//         handled = d_sampleApp->injectMouseButtonUp(CEGUI::LeftButton);
//         break;
//         //! Right mouse button was left up.
//     case EMIE_RMOUSE_LEFT_UP:
//         handled = d_sampleApp->injectMouseButtonUp(CEGUI::RightButton);
//         break;
//         //! Middle mouse button was left up.
//     case EMIE_MMOUSE_LEFT_UP:
//         handled = d_sampleApp->injectMouseButtonUp(CEGUI::MiddleButton);
//         break;
//         //! The mouse cursor changed its position.
//     case EMIE_MOUSE_MOVED:
//         handled = d_sampleApp->injectMousePosition(
//             static_cast<float>(x), static_cast<float>(y));
//         break;
//         //! The mouse wheel was moved. Use Wheel value in event data to find out
//         //! in what direction and how fast.
//     case EMIE_MOUSE_WHEEL:
//         handled = d_sampleApp->injectMouseWheelChange(w);
//         break;
//     default:
//         break;
//     }
//     return handled;

// }

//----------------------------------------------------------------------------//
void CEGuiSaga3DBaseApplication::checkWindowResize()
{
    /*
#if CEGUI_IRR_SDK_VERSION >= 16
    saga::core::dimension2d<std::uint32_t> cur_size = d_driver->getScreenSize();
#else
    saga::core::dimension2d<std::int32_t> cur_size = d_driver->getScreenSize();
#endif

    if ((static_cast<float>(cur_size.Width) != d_lastDisplaySize.d_width) ||
        (static_cast<float>(cur_size.Height) != d_lastDisplaySize.d_height))
    {
        d_lastDisplaySize.d_width = static_cast<float>(cur_size.Width);
        d_lastDisplaySize.d_height = static_cast<float>(cur_size.Height);
        CEGUI::System::getSingleton().
            notifyDisplaySizeChanged(d_lastDisplaySize);
    }
    */
}

//----------------------------------------------------------------------------//

#endif
